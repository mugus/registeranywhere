# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class contactus(models.Model):
    names = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    subject = models.CharField(max_length=1000)
    message = models.CharField(max_length=10000)

    def __str__(self):
        return self.subject

"""
class  coders(models.Model):
    MALE = 'ML'
    FEMALE = 'FM'
    CHOICES = (
        (MALE, 'Male'),
        (FEMALE, 'Female'),
    )
    names = models.CharField(max_length=100,help_text="Enter your full names")
    email = models.CharField(max_length=100,help_text="Your Email address")
    gender = models.CharField(max_length=2,choices=CHOICES,default=MALE,help_text="Gender")
    bio = models.CharField(max_length=200,help_text="Tell us something about yourself")
    username = models.CharField(max_length=10,unique=True,help_text="Prefered username ex: Ghost")
    password = models.CharField(max_length=100,help_text="Your Password")

    def __str__(self):
        return self.names
"""

class mugus(models.Model):
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=100)