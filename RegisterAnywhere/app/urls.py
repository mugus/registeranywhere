
from django.conf.urls import url

from . import views

urlpatterns = [
    url('^apply$', views.apply, name='apply'),
    url('^help$', views.help, name='help'),
    url('^login$', views.login, name='login'),
    url('^signin$', views.signin, name='signin'),
    url('^adminhome$', views.adminhome, name='adminhome'),
    url('^dig$', views.dig, name='dig'),
    url('^contact$', views.contactus_func, name='contactus'),
    url('^$', views.home, name='home'),
]
